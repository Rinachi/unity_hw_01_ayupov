﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
  private static GameState state;
  enum GameState
  {
    GAME,
    VICTORY,
    DEFEAT
  }
  public GameObject player_go;
  public GameObject exit_go;
  public GameObject marker_go;

  float start_game_stamp;
  float game_time = 30;
  
  float spawn_markers_stamp;
  float spawn_markers_cooldown = 0.5f;
  
  int left_time = 0;
  Rect screen_rect = new Rect(0, 0, 60, 60);

  Marker first_marker;
  Color exit_color;
  // Start is called before the first frame update
  void Start()
  {
    state = GameState.GAME;
    marker_go.SetActive(false);
    
    exit_color = GetRandomColor();
  
    first_marker = CreateMarker();
    first_marker.Init(exit_color, first_marker.transform.position, Vector3.zero);
    
    Player player = player_go.AddComponent<Player>();
    Exit exit = exit_go.AddComponent<Exit>();
    exit.Init(exit_color, player);
    
    start_game_stamp = Time.time;
  }

  float GetLeftTime()
  {
    return Mathf.Max(0, start_game_stamp + game_time - Time.time);
  }

  Marker CreateMarker()
  {
    GameObject marker = Instantiate(marker_go);
    marker.SetActive(true);
    return marker.AddComponent<Marker>(); 
  }
  
  // Update is called once per frame
  void Update()
  {
    if (state != GameState.GAME)
      return;
    
    if (GetLeftTime() == 0)
      GameOver(false);
    
    left_time = (int)GetLeftTime();

    if (first_marker != null || spawn_markers_stamp + spawn_markers_cooldown > Time.time)
      return;
    
    spawn_markers_stamp = Time.time;
  
    Vector3 start_pos;
    Vector3 direction;
    
    switch (Random.Range(0, 4))
    {
        case 0:
          start_pos = Vector3.left*screen_rect.width/2 + Vector3.forward*screen_rect.height/2 + Vector3.back*screen_rect.height*Random.value;
          direction = Vector3.right;
          break;
        case 1:
          start_pos = Vector3.right*screen_rect.width/2 + Vector3.forward*screen_rect.height/2 + Vector3.back*screen_rect.height*Random.value;
          direction = Vector3.left;
          break;
        case 2:
          start_pos = Vector3.left*screen_rect.width/2 + Vector3.forward*screen_rect.height/2 + Vector3.right*screen_rect.width*Random.value;
          direction = Vector3.back;
          break;
        default :
          start_pos = Vector3.left*screen_rect.width/2 + Vector3.back*screen_rect.height/2 + Vector3.right*screen_rect.width*Random.value;
          direction = Vector3.forward;
          break;
    }
    Color color = GetRandomColor();
    Marker random_marker = CreateMarker();
    random_marker.Init(color, start_pos, direction);
  }

  private Color GetRandomColor()
  {
    return new List<Color>{Color.red,Color.green,Color.blue}[Random.Range(0,3)];
  }

  void OnGUI() {
    if (IsGame())
    {
      GUILayout.BeginHorizontal();
      GUI.skin.label.alignment = TextAnchor.UpperLeft;
      GUI.skin.label.normal.textColor = Color.white;
      GUI.Label(new Rect(10, 10, 100, 20), "Left Time: " + left_time.ToString());
      GUI.skin.label.alignment = TextAnchor.UpperRight;
      GUI.skin.label.normal.textColor = exit_color;
      GUI.Label(new Rect(Screen.width - 410, 10, 400, 20), "Increase color intensity(small cubes) for exit(big one)!");
      GUILayout.EndHorizontal();
    }
    else
    {
      GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
      GUI.Box (new Rect (0,0,100,100), state == GameState.VICTORY ? "VICTORY" : "DEFEAT");
      if (GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
      }
      GUI.EndGroup ();
    }
  }
  
  public static bool IsGame()
  {
    return state == GameState.GAME;
  }
  
  public static void GameOver(bool is_victory)
  {
    state = is_victory ? GameState.VICTORY : GameState.DEFEAT;
  }
}
